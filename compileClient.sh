#!/bin/bash
cd "${0%/*}"/com/company
echo "*** Compiling the client side simulator ***"
javac *.java
echo "*** Client side simulator compiled successfully! ***"
