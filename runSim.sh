#!/bin/bash
outDir="${0%/*}/logs"

if [[ ! -d ${outDir} ]]; then
	mkdir "${0%/*}/logs"
fi

if [ "$1" = "-h" ]; then 
	echo "	Usage is ./runSim.sh <config file> <algorithm> <output file>"
fi

if [ ! "$1" = "-h" ]; then
	if [ "$#" -eq 1 ]; then
		echo "*** Running the simulation with $1 and atl ***"
		cd "${0%/*}"
		./ds-server -c configs/$1 -v brief -n & 
		sleep 2	
		java com.company.Main 
	fi
	if [ "$#" -eq 2 ]; then
		echo "*** Running the simulation with $1 and $2 ***"	
		cd "${0%/*}"	
		./ds-server -c configs/$1 -v brief -n &
		sleep 2	
		java com.company.Main -a $2
	fi
	if [ "$#" -eq 3 ]; then
		echo "*** Running the simulation with $1 and $2 ***"	
		cd "${0%/*}"	
		./ds-server -c configs/$1 -v brief -n > "logs/$3.log" &
		sleep 2	
		java com.company.Main -a $2
	fi
fi
